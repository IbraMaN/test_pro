Django==3.2
psycopg2-binary>=2.8
django-rest-swagger==2.2.0
djangorestframework==3.13.1
djangorestframework-swagger==0.2.2
drf-yasg==1.21.3
celery==5.1.2
redis==3.5.3
