from rest_framework import serializers

from .models import Client, Notification, Message


class ClientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class NotificationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'


class NotificationsStatiscticsNotificationSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = Notification
        fields = ['id', 
                  'filter_clients', 
                  'text', 
                  'start_datetime', 
                  'stop_datetime',
        ]


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MessageStatiscticsNotificationSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = Message
        fields = ['id', 
                  'client_id', 
                  'status_code',
                  'status_text', 
                  'datetime_create',
        ]
