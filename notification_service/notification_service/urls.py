from django.urls import path, include

from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from .views import ( 
    Clients, 
    Notifications, 
    statistics_notifications,
    statistics_notification, 
    active_notifications
)


app_name = 'notification_service'

router = DefaultRouter()
router.register('client', Clients, basename='client')
router.register('notification', Notifications, basename='notification')
urlpatterns = router.urls
urlpatterns += [
    path('statistics/', statistics_notifications),
    path('statistics/<int:notification_id>/', statistics_notification),
    path('active_notifications/', active_notifications)
]





