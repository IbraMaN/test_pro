from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponse, JsonResponse
from django.core.serializers import serialize
from django.utils.dateparse import parse_datetime
from django.db.models import Q
from django.http import Http404

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_swagger import renderers
from rest_framework.decorators import api_view, renderer_classes

from .models import (
    Client, 
    Notification, 
    Message
)
from .serializers import (
    ClientsSerializer, 
    NotificationsSerializer,
    NotificationsStatiscticsNotificationSerializer,
    MessageSerializer,
    MessageStatiscticsNotificationSerializer,
) 
from .tasks import send_message_clients

import re
from datetime import datetime


@api_view(['GET'])
@renderer_classes([renderers.OpenAPIRenderer, 
                   renderers.SwaggerUIRenderer])
def statistics_notifications(request):
    """Getting brief statistics of notifications
    
    return statisctics notifications
    """
    messages = Message.objects.all()
    statistics = []
    for item in Notification.objects.all():
        current_messages = messages.filter(notification=item.id)
        count_current_messages = current_messages.count()
        count_sent_current_messages = current_messages.filter(
            status_code=200
        ).count()
        count_delivered_current_messages = (
            count_current_messages - count_sent_current_messages
        )
        statistics.append({
            # unpacking attribut 'Notification'
            **NotificationsStatiscticsNotificationSerializer(
                item
            ).data,
            'messages': {
                'count': count_current_messages,
                'sent_count': count_sent_current_messages,
                'delivered_count': count_delivered_current_messages,
            },
        })
    return JsonResponse({'statistics':statistics})


@api_view(['GET'])
@renderer_classes([renderers.OpenAPIRenderer, 
                   renderers.SwaggerUIRenderer])
def statistics_notification(request, notification_id: int):
    """ Getting statistics of notification

    :param notification_id:
    :type int:
    """

    try:
        notification = Notification.objects.get(id=notification_id)
    except Notification.DoesNotExist:
        return HttpResponse(status=404)
    dict_notification = NotificationsStatiscticsNotificationSerializer(
        notification,
    ).data
    messages_delivered = Message.objects.filter(
        notification__id=notification_id,
        status_code=200,
    )
    messages_sent = Message.objects.filter(
        Q(notification__id=notification_id) & ~Q(status_code=200)
    )
    dict_messages_delivered = MessageStatiscticsNotificationSerializer(
        messages_delivered,
        many=True,
    ).data
    dict_messages_sent = MessageStatiscticsNotificationSerializer(
        messages_sent,
        many=True,
    ).data
    count_delivered_messages = len(dict_messages_delivered)
    count_sent_messages = len(dict_messages_sent)
    return JsonResponse({'notification':{ 
        # unpacking attribut 'Notification'
        **dict_notification,
        'messages': {
            'count': count_sent_messages + count_delivered_messages,
            'delivered':{
                'count': count_delivered_messages,
                'messages': dict_messages_delivered
            },
            'sent': {
                'count': count_sent_messages,
                'messages': dict_messages_sent
            },
        },
    }})


@api_view(['GET'])
@renderer_classes([renderers.OpenAPIRenderer, 
                   renderers.SwaggerUIRenderer])
def active_notifications(request):    
    """Getting active notifications
    
    return active notifications
    """

    datetime_now = datetime.now()
    active_notifications = Notification.objects.filter(
        Q(stop_datetime__gte=datetime_now) 
        & Q(start_datetime__lte=datetime_now)
        )
    
    if active_notifications:
        statistics = {
            item.id:send_message_clients(item.id)
            for item in active_notifications
            if not (
                (_ := Message.objects.filter(notification__id=item.id))
                and _.latest().status_code != 200
                )
        }
    else: statistics = 'No active notifications'
            
    return JsonResponse({'message':statistics})


class Notifications(viewsets.ModelViewSet):
    serializer_class = NotificationsSerializer
    queryset = Notification.objects.all()
    permission_classes = (IsAuthenticated,)

    def create(self, request):
        data = {'start_datetime':request.data.get('start_datetime'),
                'text':request.data.get('text'),
                'filter_clients':request.data.get('filter_clients'),
                'stop_datetime':request.data.get('stop_datetime')
            }
        format = "%Y-%m-%dT%H:%M:%S.%fZ"

        try:
            data['start_datetime'] = datetime.strptime(
                data.get('start_datetime'), 
                format
                )
            data['stop_datetime'] = datetime.strptime(
                data.get('stop_datetime'), 
                format
                )
        except ValueError:
            return JsonResponse({'Status':'Wrong format datatime'})
        notification_create=Notification(**data)
        notification_create.save()
        if (data.get('start_datetime') 
            <= datetime.now() 
            <= data.get('stop_datetime')
            ):
            return JsonResponse({
                'Success':send_message_clients(notification_create.id)
                })
        elif (data.get('start_datetime') 
              >= datetime.now() 
              <= data.get('stop_datetime')
            ):
            send_message_clients.apply_async(
                (notification_create.id,), 
                eta=data.get('start_datetime')
            )
            return JsonResponse({
                'Success':'Notifications will be sent later'
                })
        else: 
            return JsonResponse({
                'Success':'Notifications date is outdated'
                })


class Clients(viewsets.ModelViewSet):
    serializer_class = ClientsSerializer
    queryset = Client.objects.all()
    permission_classes = (IsAuthenticated,)