from django.db import models


class Client(models.Model):
    code_operator = models.CharField(
        'Код оператора', 
        max_length=6,
    )
    phone_number = models.CharField(
        'Номер телефона', 
        max_length=10,
    )
    tag = models.CharField(
        'Тег', 
        max_length=200,
    )
    timezone = models.CharField(
        'Часовой пояс',
        max_length=200,
    )

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Notification(models.Model):
    filter_clients = models.CharField(
        'Фильтер клиентов', 
        max_length=200,
    )
    text = models.TextField('Текст рассылки')
    start_datetime = models.DateTimeField('Дата начала')
    stop_datetime = models.DateTimeField('Дата остановки')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Message(models.Model):
    client = models.ForeignKey(
        Client,
        verbose_name='client_id',
        on_delete = models.CASCADE,
        null=0,
        related_name='message',
    )
    notification = models.ForeignKey(
        Notification,
        verbose_name='notification_id',
        on_delete = models.CASCADE,
        null=0,
        related_name='message',
    )
    status_code = models.IntegerField()
    status_text = models.CharField(
        'Статус', 
        max_length=100, 
        blank=True,
    )
    datetime_create = models.DateTimeField(
        'Дата создания', 
        auto_now_add=True,
    )

    class Meta:
        get_latest_by = "datetime_create"
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'