from celery import shared_task
from celery.utils.log import get_logger
from test_serv.celery import app

from django.db.models import Q
from django.http import HttpResponse

from .models import Client, Notification, Message

from test_serv.settings import URL_SEND_SERVICE, TOKEN_SEND_SERVICE

import requests
import json
from datetime import datetime, timedelta


logger = get_logger(__name__)


@app.task
@shared_task
def send_message_clients(notification_id: int) -> dict:
    m_bad = False
    m_count, m_delivered = 0, 0
    try:
        notification = Notification.objects.get(id=notification_id)
    except ValueError:
        return {'Notification id:'+str(notification_id):'Not found'}
    for item in Client.objects.filter(
            Q(tag=notification.filter_clients)
            | Q(code_operator=notification.filter_clients)
        ):
        res = send_message(
            item.id, 
            item.code_operator, 
            item.phone_number, 
            notification.text,
        )
        data = {
            'client':item,
            'notification':notification,
            'status_code': res.status_code,
            'datetime_create':datetime.now(),
        }
        m_count += 1
        if res.status_code == 200: 
            m_delivered += 1
        else: 
            m_bad = True
            data['status_text'] = res.text
        m_current = Message(**data)
        m_current.save()

    date_next_send_msg = datetime.now() + timedelta(minutes=5)
    if m_bad and (notification.stop_datetime >= date_next_send_msg):
        send_message_clients.apply_async(
            (notification_id,),
            eta=date_next_send_msg,
        )
    return {
        'all': m_count,
        'delivered': m_delivered,
        'sent': m_count - m_delivered,
    }


def send_message(client_id: int, 
                 code_operator: str, 
                 phone_number: str, 
                 m_text: str
                ):
    try:
        return requests.post(
            URL_SEND_SERVICE + str(client_id),
            json = {
                "id": client_id,
                "phone": int(code_operator + phone_number),
                "text": m_text,
            },
            headers = TOKEN_SEND_SERVICE,
        )
    except: 
        HR = HttpResponse()
        HR.status_code = 'not connect to service'
        return HR