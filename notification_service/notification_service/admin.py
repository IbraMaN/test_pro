from django.contrib import admin
from .models import Client, Notification, Message

#class ClientAdmin(admin.ModelAdmin):
#    ordering = ['-created']
#    list_display = all # ('phone_numbers', 'created', 'available', 'id')
#    list_display_links = ('name',)
#    list_editable = ('available',)


admin.site.register(Client)
admin.site.register(Notification)
admin.site.register(Message)
